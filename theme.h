# define DMENU_TOPBAR 1
# define DMENU_FONTS {"IBM Plex Mono", "DejaVu Sans Mono", "monospace", "JoyPixels:size=10:antialias=true:autohint=true", "Symbols Nerd Font:style=2048-em:size=10:antialias=true:autohint=true", "Noto Color Emoji:size=10:style=Regular"}
# define DMENU_FOREGROUND "#839496"
# define DMENU_BACKGROUND "#002b36"
# define DMENU_SELFOREGROUND "#fdf6e3"
# define DMENU_SELBACKGROUND "#073642"
# define DMENU_OUTFOREGROUND "#000000"
# define DMENU_OUTBACKGROUND "#00ffff"
